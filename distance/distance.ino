#define TRIGGER   12
#define ECHO      13
#define GREEN_LED 11
#define RED_LED   10

double distance;

void setup()
{
  Serial.begin(9600);
  Serial.print("Started!!!\n\r");

  pinMode(GREEN_LED, OUTPUT);
  pinMode(RED_LED,   OUTPUT);
  pinMode(TRIGGER,   OUTPUT);
  pinMode(ECHO,      INPUT);

  digitalWrite(GREEN_LED, HIGH);
}

void loop()
{
  digitalWrite(TRIGGER, LOW);
  delayMicroseconds(2);
  digitalWrite(TRIGGER, HIGH);
  delayMicroseconds(10);
  digitalWrite(TRIGGER, LOW);
  distance = pulseIn(ECHO, HIGH, 36000) / 58.0;

  if (distance > 20) {
      digitalWrite(RED_LED,   LOW);
      digitalWrite(GREEN_LED, HIGH);
  }
  else {
      digitalWrite(RED_LED,   HIGH);
      digitalWrite(GREEN_LED, LOW);
  }

  Serial.print("\nDistance in centimers: ");
  Serial.print((int)distance);
  delay(200);
}
