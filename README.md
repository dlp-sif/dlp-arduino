Demandez le Programme
=====================

Cette neuvième session de Demandez le Programme est consacrée à une
découverte d'Arduino.

[Documentation](https://www.arduino.cc/en/Reference/HomePage)


Liste des exercices
-------------------

1. [Les bases](basic): faites clignoter une LED et actionnez la à l'aide d'un
   bouton poussoir
2. [Le principe de la PWM (MLI)](led_pwm): utilisez la PWM (Pulse Width
   Modulation) pour changer l'intensité lumineuse d'une LED
3. [Piloter un servomoteur](servo): utilisez la bibliothèque Servo pour
   contrôler un servomoteur
4. [Contrôle d'une LED par Bluetooth](led_bluetooth): envoyez des valeurs d'intensité
   par bluetooth pour contrôle une LED
5. [Capteur de distance à ultrasons](distance): réalisez un système de detection
   d'obstacle avec un capteur à ultrasons
6. [Utilisation d'un afficheur LCD](lcd_display): contrôlez un afficheur LCD
7. [Lecture d'un encodeur rotatif](rotary_encoder): lire un encodeur rotatif
   (utilisation des interruptions)
