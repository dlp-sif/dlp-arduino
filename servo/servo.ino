/*
 * Contrôler un Servomoteur
 */
#include <Servo.h>  /* Bibliothèque de contrôle de servomoteurs */

#define SERVO_PIN (12U) /* On attache le servomoteur à la prise 12 de l'Arduino */

Servo myservo; /* On créé l'objet servomoteur */

void setup() {
    myservo.attach(SERVO_PIN); 
    myservo.write(90);  /* Position du milieu du servomoteur (les valeurs vont de 0 à 180) */
}

void loop() {
    delay(2000);        /* 2 secondes d'attente */
    myservo.write(20);  /* Le servomoteur va à la position 20 */
    delay(2000);
    myservo.write(120);
    delay(4000);
    myservo.write(90);
}
