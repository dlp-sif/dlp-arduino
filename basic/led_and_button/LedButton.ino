/*
  Allumer une LED avec un bouton !
 */

/* Brancher l'anode de la LED (la patte longue) sur la prise 13 protégée par une résistance d'1kOhm.
   La cathode est reliée directement à la masse. */
int led = 13;

/* Brancher l'un des coté du bouton à la prise 5V. L'autre coté est relié à la masse à travers une
   résistance de "tirage" aussi appelée "pull-down". */
int button = 7;

/* Variable global contenant l'état du bouton. */
int pressed = 0;


/* La fonction "setup" initialize les prises de carte. Ici:
 - la prise "led" est une sortie, "OUTPUT"
 - la prise "bouton" est une entrée, "INPUT"
 */
void setup() {
  pinMode(led, OUTPUT);
  pinMode(button, INPUT);
}


/* La fonction "loop" boucle indéfiniment tant que la carte est allumée. */
void loop() {

  /* on lit l'état de la prise "bouton": est-il pressé ? */
  pressed = digitalRead(button);
  if (pressed == HIGH) { /* Oui */
    digitalWrite(led, HIGH); /* La LED est allumée */
  }
  else { /* Non */
    digitalWrite(led, LOW); /* La LED est éteinte */
  }
}
