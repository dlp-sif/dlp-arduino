/*
  Allumer une LED avec un bouton !
 */
 
/* Brancher l'anode de la LED (la patte longue) sur la prise 13 protégée par une résistance d'1kOhm. 
   La cathode est reliée directement à la masse. */
int led = 13;

/* La fonction "setup" initialize les prises de carte. Ici la prise "led" est une sortie, "OUTPUT"
 */
void setup() {
  pinMode(led, OUTPUT);
}


/* La fonction "loop" boucle indéfiniment tant que la carte est allumée. */
void loop() {
  digitalWrite(led, HIGH);   /* LED allumée */
  delay(1000);               /* 1 seconde d'attente */
  digitalWrite(led, LOW);    /* LED éteinte */
  delay(1000);               /* 1 seconde d'attente */
}

/* 
1. Chargez ce programme sur la carte en cliquant sur la flèche horizontale
2. La LED rouge clignote. Que constatez-vous d'autre sur la carte ?
3. Faites varier le temps d'attente entre chaque allumage/arret. Que constatez-vous pour les valeurs inférieures à 10 (ms) ? Pourquoi ?
4. Pour un cycle durant 20ms, faites variez la durée de chaque état de telle sorte que <temps LED allumée> + <temps LED éteinte> = 20 ms. Que constatez vous ?
*/
