#include<SoftwareSerial.h>

/*
 * Faire varier l'intensité d'une LED avec une liaison 
 * bluetooth.
 *
 * Conversion basee sur https://www.arduino.cc/en/Tutorial/StringToIntExample
 * (domaine public) 
 */
const int PWM_PORT = 10; 
const int RX_PORT = 2;
const int TX_PORT = 3;
SoftwareSerial link(RX_PORT, TX_PORT);
int brightness = 40;
String inputValue;

void setup(){
  pinMode(PWM_PORT, OUTPUT);
  link.begin(9600);
}

void loop(){
    while (link.available() > 0) {
    int inChar = link.read();
    if (isDigit(inChar)) {
      inputValue += (char)inChar;
    }
    if (inChar == '\n') {
      brightness = inputValue.toInt();
      inputValue = "";
    }
  }
  analogWrite(PWM_PORT,brightness);
  delay(20);
}
