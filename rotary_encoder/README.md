Lecture d'un encodeur rotatif
=============================

Pour une explication du fonctionnement d'un encodeur rotatif, voir [la page du playground Arduino](http://playground.arduino.cc/Main/RotaryEncoders)

## Schéma de principe

![principe de l'encodeur rotatif](RotaryEncoderWaveform.gif)

