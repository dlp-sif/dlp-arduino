/*
 * Faire varier l'intensité d'une LED
 */
void setup() {}

const int PWM_PORT = 10; 
int brightness = 40;
int delta = 5;

void loop(){
  int bpd = brightness + delta;
  if(bpd < 1 || bpd > 125){
    delta = -delta;
  }
  brightness += delta;
  analogWrite(PWM_PORT,brightness);
  delay(20);
}
