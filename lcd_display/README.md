### Utilisation de Liquid Crystal Display avec Arduino

On se propose ici d'utiliser un module LCD avec la carte Arduino. Il s'agira de
simplement utiliser et modifier les exemples donnés dans l'environnment de
développement Arduino.

#### Mise en place du montage

Faire les branchements de la carte et du module LCD en suivant le schéma donné
par ce [tutoriel
Arduino](https://www.arduino.cc/en/Tutorial/LiquidCrystalDisplay).

#### Exercices

1. Sélectionnez le sketch d'exemple "LiquidCrystal/Display" depuis
   l'environnement de développement Arduino et "téléversez" le sur la carte.
   Réglez le potentiomètre jusqu'à voir apparaitre le message clignotant "hello
   world"

2. Testez l'exemple "LiquidCrystal/SerialDisplay" et envoyez des messages au
   module d'affichage

3. Adaptez le montage et le sketch pour inclure le module Bluetooth et envoyez
   des messages depuis votre téléphone
